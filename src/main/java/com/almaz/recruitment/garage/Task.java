package com.almaz.recruitment.garage;

public class Task {

    private Car car;
    private Job job;
    private int duration;

    public Task(Car car, Job job, int duration) {
        this.car = car;
        this.job = job;
        this.duration = duration;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public Job getJob() {
        return job;
    }

    public void setJob(Job job) {
        this.job = job;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }
}
