package com.almaz.recruitment.garage;

/*
 * Copyright (c) 2016 Almaz Informatique SA
 *
 * The regular car is cheap to maintain. This is how long the service jobs will take:
 * SERVICE       2 hrs
 * CHECKUP       3 hrs
 * TROUBLESHOOT  4 hrs
 * PAINT         2 hrs over 3 days (6 hrs total)
 *
 * The german car is more expensive to maintain. This is how long the service jobs will take:
 * SERVICE       4 hrs
 * CHECKUP       6 hrs
 * TROUBLESHOOT  8 hrs
 * PAINT         2 hrs over 3 days (6 hrs total)
 */
public enum Car {

    GERMAN_CAR {
        @Override
        int carComplexity() {
            return 2;
        }
    },
    REGULAR_CAR {
        @Override
        int carComplexity() {
            return 1;
        }
    };

    abstract int carComplexity();
}
