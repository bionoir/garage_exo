package com.almaz.recruitment.garage;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

public class GeneralGarage {

    protected int workHoursPerDay;

    protected TreeMap<Integer, Agenda> pipelinesAgenda;

    protected List<Task> newTasks;

    protected int scheduleTaskOnPipeline(Integer pipeline, int dayOfAgenda) throws UnsupportedOperationException {

        int indexOfTaskList = newTasks.size() - 1;

        Agenda pipelineAgenda = pipelinesAgenda.get(pipeline);
        List<Task> dailyTasks = pipelineAgenda.getScheduledTasks().get(dayOfAgenda);

        if (dailyTasks == null) dailyTasks = new ArrayList<>();
        int hoursLeftForDay = workHoursPerDay - dailyTasks.stream().mapToInt(task -> task.getDuration()).sum();

        if (hoursLeftForDay > 0) {
            Task taskToSchedule = newTasks.get(indexOfTaskList);
            int taskDuration = taskToSchedule.getDuration();

            if (hoursLeftForDay < taskDuration) {
                Task newTaskForTheDay = new Task(taskToSchedule.getCar(),
                                                taskToSchedule.getJob(),
                                                hoursLeftForDay);
                pipelineAgenda.addTaskForDay(dayOfAgenda, newTaskForTheDay);
                taskToSchedule.setDuration(taskDuration - hoursLeftForDay);

            } else {
                Task newTaskForTheDay = new Task(taskToSchedule.getCar(),
                                                taskToSchedule.getJob(),
                                                taskDuration);
                pipelineAgenda.addTaskForDay(dayOfAgenda, newTaskForTheDay);

                if (newTasks.size() > 1) {
                    dayOfAgenda += 1;
                }
                newTasks.remove(indexOfTaskList);
            }
        } else {
            dayOfAgenda += 1;
        }

        return dayOfAgenda;

    }
}
