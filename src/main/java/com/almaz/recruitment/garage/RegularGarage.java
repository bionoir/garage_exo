package com.almaz.recruitment.garage;

import java.util.*;

/*
 * Copyright (c) 2016 Almaz Informatique SA
 *
 * The regular garage can work on one car at a time, and has a limited number of working hours per day.
 */
public class RegularGarage extends GeneralGarage implements Garage {

    private final int pipelines = 0;

    public RegularGarage(int hours) {
        this.workHoursPerDay = hours;
        this.pipelinesAgenda = new TreeMap<>();
        this.pipelinesAgenda.put(this.pipelines, new Agenda());
        this.newTasks = new ArrayList<>();
    }

    public int schedule(Car car, Job job) throws UnsupportedOperationException {

        int jobDuration = job.jobDuration(car);
        int numOfTasks = job.splittedJob();
        int hoursForEachSubTask = jobDuration / numOfTasks;
        int whenCarReady = 0;

        for(int i = 0; i < numOfTasks; i++) {
            newTasks.add(new Task(car,job,hoursForEachSubTask));
        }

        while( newTasks.size() != 0 ) {
            whenCarReady = scheduleTaskOnPipeline(this.pipelines, whenCarReady);
        }

        return whenCarReady;
    }

}
