package com.almaz.recruitment.garage;

/*
 * Copyright (c) 2016 Almaz Informatique SA
 *
 * The jobs have different durations, which depends on the type of car. Unless otherwise specified, the
 * jobs can be split over several days.
 * For example: the troubleshooting takes 6 hours, and can be performed over 1 day (6 hrs), 2 days (2 hrs, 4 hrs),
 * 3 days (2 hrs, 1 hrs, 3 hrs), etc
 *
 * The paint job is different. It has different steps spread over several days. Each step must be finished in one day!
 * For example: a regular paint job takes 2 hours over 3 days, and can be performed in 3 days (2 hrs, 2 hrs, 2 hrs),
 * 4 days (2 hrs, 0 hrs, 2 hrs, 2 hrs), 5 days (2 hrs, 0, 2 hrs, 0, 2 hrs), etc
 */
public enum Job {

    SERVICE {
        @Override
        int jobDuration(Car car) {
            return car.carComplexity() * 2;
        }

        @Override
        int splittedJob() {
            return 1;
        }
    },
    CHECKUP {
        @Override
        int jobDuration(Car car) {
            return car.carComplexity() * 3;
        }

        @Override
        int splittedJob() {
            return 1;
        }
    },
    TROUBLESHOOT {
        @Override
        int jobDuration(Car car) {
            return car.carComplexity() * 4;
        }

        @Override
        int splittedJob() {
            return 1;
        }
    },
    PAINT {
        @Override
        int jobDuration(Car car) {
            return 6;
        }

        @Override
        int splittedJob() {
            return 3;
        }
    };

    abstract int jobDuration(Car car);
    abstract int splittedJob();
};



