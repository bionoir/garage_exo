package com.almaz.recruitment.garage;

import java.util.*;

public class Agenda {

    private TreeMap<Integer, List<Task>> scheduledTasks;

    public Agenda() {
        scheduledTasks = new TreeMap<>();
        List<Task> newTaskList = new ArrayList<>();
        scheduledTasks.put(0, newTaskList);
    }

    public void addTaskForDay(Integer day, Task newTask) {
        List<Task> taskList = this.scheduledTasks.get(day);
        if (taskList == null) {
            taskList = new ArrayList<>();
        }
        taskList.add(newTask);
        scheduledTasks.put(day, taskList);
    }

    public TreeMap<Integer, List<Task>> getScheduledTasks() {
        return this.scheduledTasks;
    }
}
