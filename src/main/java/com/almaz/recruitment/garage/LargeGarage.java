package com.almaz.recruitment.garage;

import java.util.*;

/*
 * Copyright (c) 2016 Almaz Informatique SA
 *
 * The large garage has a limited number of working hours per day. Compared to other garages,
 * it has several pipelines, allowing it to work on several cars at the same time.
 *
 * Once a job is started, the job cannot switch pipelines. The less busy pipeline will be chosen.
 */
public class LargeGarage extends GeneralGarage implements Garage {

    private final int pipelines;

    public LargeGarage(int hours, int pipelines) {
        this.workHoursPerDay = hours;
        this.pipelines = pipelines;
        this.pipelinesAgenda = new TreeMap<>();
        for (int i = 0; i < pipelines; i++) {
            Agenda agendaForPipeline = new Agenda();
            pipelinesAgenda.put(i, agendaForPipeline);
        }

        this.newTasks = new ArrayList<>();
    }

    public int schedule(Car car, Job job) throws UnsupportedOperationException {
        int jobDuration = job.jobDuration(car);
        int numOfTasks = job.splittedJob();
        int hoursForEachSubTask = jobDuration / numOfTasks;
        int whenCarReady = 0;

        for(int i = 0; i < numOfTasks; i++) {
            newTasks.add(new Task(car,job,hoursForEachSubTask));
        }

        Integer pipelineForTask = findLessBusyPipeline();
        if (pipelineForTask == null) {
            throw new UnsupportedOperationException("The pipeline was not found! Initialization problem ?");
        }

        while(newTasks.size() > 0) {
            whenCarReady = scheduleTaskOnPipeline(pipelineForTask, whenCarReady);
        }

        return whenCarReady;
    }

    // TODO - To refactor, optimize
    private Integer findLessBusyPipeline() {

        Map<Integer, Map<Integer, Integer>> pipelineOccupations = new HashMap<>();

        Integer pipeline = 0;
        Integer dayFound = Integer.MAX_VALUE;
        Integer hoursAvailable = workHoursPerDay;

        // Create a multi-table to summarize the timeschedule for each pipeline,
        for(Map.Entry<Integer, Agenda> entry: pipelinesAgenda.entrySet() ) {
            Map<Integer, Integer> listPerDayOfHoursFree = new HashMap<>();
            for (Map.Entry<Integer, List<Task>> agendaEntries : entry.getValue().getScheduledTasks().entrySet()) {
                int plannedHoursForDay = agendaEntries.getValue().stream().mapToInt(task -> task.getDuration()).sum();
                listPerDayOfHoursFree.put(agendaEntries.getKey(), workHoursPerDay - plannedHoursForDay);
            }

            // then find the tuple with the pipeline, the day and hours remaining
            Integer minimum = workHoursPerDay;
            System.out.println(listPerDayOfHoursFree);
            for (Map.Entry<Integer, Integer> days : listPerDayOfHoursFree.entrySet()) {
                if (days.getValue() <= minimum) {
                    Integer key = days.getKey();
                    Integer value = days.getValue();
                    Map<Integer, Integer> dayHoursMap = new HashMap<>();
                    dayHoursMap.put(key, value);
                    pipelineOccupations.put(entry.getKey(), dayHoursMap);
                }
            }
        }

        // final loop to get the optimum day in order to schedule the next task
        for(Map.Entry<Integer,Map<Integer,Integer>> pipelinesEntries: pipelineOccupations.entrySet()) {
            Integer pipelineTested = pipelinesEntries.getKey();
            Integer dayFoundTested = pipelinesEntries.getValue().entrySet().iterator().next().getKey();
            Integer hourFoundTested = pipelinesEntries.getValue().entrySet().iterator().next().getValue();

            if (dayFoundTested < dayFound) {
                dayFound = dayFoundTested;
                hoursAvailable = hourFoundTested;
                pipeline = pipelineTested;
            } else if (dayFoundTested.equals(dayFound)) {
                if (hourFoundTested > hoursAvailable) {
                    hoursAvailable = hourFoundTested;
                    pipeline = pipelineTested;
                }
            }
        }

        return pipeline;
    }

}
