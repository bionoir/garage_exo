package com.almaz.recruitment.garage;

/*
 * Copyright (c) 2016 Almaz Informatique SA
 *
 * This simple garage has no capacity or working hour constraints, it can work on any number of cars at the same time.
 */
public class SimpleGarage implements Garage {

    public int schedule(Car car, Job job) throws UnsupportedOperationException {

        if (car == null || job == null) {
            throw new UnsupportedOperationException("Missing information : the schedule job cannot be created!");
        }

        switch (job) {
            case PAINT:
                return 2;
            case SERVICE:
            case TROUBLESHOOT:
            case CHECKUP:
                return car.carComplexity() - 1;
            default:
                throw new UnsupportedOperationException("The garage service you're trying to create, is still not existing");

        }
    }

}