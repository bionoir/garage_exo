## Garage planner

Bob has some money and an idea: he wants to enter the software business with an offering for car services (garages).
Unfortunately for Bob, the car service business is complex, each garage has its own mode of operation, so Bob must
come up with a flexible software package, that can be used by almost any garage.

In order to prepare the first version of his software, Bob has observed several car services and collected a number
of requirements. Since he's no good at writing actual code, he has hired you to implement the software for him.

Luckily for you, Bob has been able to translate the requirements into unit tests and some well-documented skeleton
classes. Read the documentation carefully, and write the code to pass the unit tests. You're not allowed to change 
the Garage interface or any of the unit tests, and Bob is not available for clarifications, he left on vacation and 
expects some working code when he gets back.

Before leaving, Bob has decided to motivate you, by offering a bonus if the resulting software is easily extensible.
New types of garages, cars or jobs should not be hard to add. Bob secretly hopes to be able to do that by himself!


## Note
The AlmaZ bitbutcket is read only for you. In order to submit changes, please send us a bundle, a zip or a link to your own cloned repository.

